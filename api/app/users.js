const express = require('express');
const User = require('../models/User');
const bcrypt = require("bcrypt");
const auth = require("../middleware/auth");
const config = require('../config');
const { OAuth2Client } = require('google-auth-library');
const clientGoogle = new OAuth2Client(config.google.clientId);
const axios = require("axios");
const {nanoid} = require("nanoid");
const router = express.Router();

//don't forget to delete it after
router.get('/', async (req, res) =>{
  try{
    const users = await User.find();
    res.send(users);
  }catch (e){
    res.sendStatus(500);
  }
});


router.post('/', async (req, res) => {
  try {
    const userData = {
      username: req.body.email,
      password: req.body.password,
      display_name: req.body.display_name,
    };

    const user = new User(userData);

    user.generateToken();
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});
router.post('/googleLogin', async (req, res) => {
  try {
    const ticket = await clientGoogle.verifyIdToken({
      idToken: req.body.tokenId,
      audience: config.google.clientId,
    });

    const {name, email, picture, sub: ticketUserId} = ticket.getPayload();

    if (req.body.googleId !== ticketUserId) {
      return res.status(401).send({global: 'User ID incorrect!'});
    }

    let user = await User.findOne({username: email});

    if (!user) {
      user = new User({
        username: email,
        password: nanoid(),
        display_name: name,
        avatar: picture,
      });
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});

    res.send({message: 'Success', user});
  } catch (error) {
    res.status(500).send({global: 'Server error. Please try again!'});
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});
  if (!user) {
    return res.status(401).send({message: "Credentials are wrong"});
  }
  const isMatch = await user.checkPassword(req.body.password);
  if (!isMatch) {
    return res.status(401).send({message: "Credentials are wrong"});
  }
  try {
    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send({message: "Username and password correct!", user});
  } catch (e) {
    res.sendStatus(500);
  }
});




router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};
  if (!token) return res.send(success);
  const user = await User.findOne({token});
  if (!user) return res.send(success);
  user.generateToken();
  await user.save({validateBeforeSave: false});
  return res.send(success);
})
module.exports = router;