const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Cocktail = require('../models/Cocktail');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();


router.get('/my', auth, async (req,res)=>{
  try{

    const myCocktails = await Cocktail.find({user: req.user._id});
    res.send(myCocktails);

  }catch (error){
    res.send(error);
  }
})

router.get('/',async (req, res) => {
  try {
    const token = req.get('Authorization');
    const user = await User.findOne({token: token});

    let cocktails;

    if (token && user && user.role === 'admin') {
      cocktails = await Cocktail.find();
      res.send(cocktails);
    } else {
      cocktails = await Cocktail.find({published: true});
      res.send(cocktails);
    }

    res.send(cocktails);

  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    if (cocktail) {
      res.send(cocktail);
    } else {
      res.status(404).send({error: "Cocktail not found"});
    }

  } catch {
    res.sendStatus(500);
  }
});



router.post('/', auth, upload.single('image'), async (req, res) => {
  try {

    const cocktailData = {
      user: req.user._id,
      title: req.body.title,
      description: req.body.description || null,
      published: false,
      category: req.body.category || null,
      ingredients: JSON.parse(req.body.ingredients),
    };

    if (req.file) {
      cocktailData.image = 'uploads/' + req.file.filename
    }

    const cocktail = new Cocktail(cocktailData);

    await cocktail.save();
    res.send(cocktail);

  } catch(error) {
    res.status(400).send({error})
  }

});


router.post('/:id/publish', auth, upload.single('image'), permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    if (Object.keys(cocktail).length === 0) {
      return res.status(404).send({error: 'Cocktail is not found.'});
    } else {
      cocktail.published = !cocktail.published;
      await cocktail.save({validateBeforeSave: false});
      return  res.send({message: `Cocktail "${cocktail.title}" was successfully published.`})
    }

  } catch (error) {
    res.status(404).send(error);
  }
});


router.delete('/:id', async (req, res) => {
  try {
    const cocktail = await Cocktail.findByIdAndDelete(req.params.id);

    if (cocktail) {
      res.send(`Cocktail '${cocktail.title} removed'`);
    } else {
      res.status(404).send({error: 'Cocktail not found'});
    }

  } catch (e) {
    res.sendStatus(500);
  }
})


module.exports = router;