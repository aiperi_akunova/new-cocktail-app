const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const IngredientSchema = new mongoose.Schema({
  title: String,
  amount: String,
})

const CocktailSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: String,
  image: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  published: {
    type: Boolean,
    default: false,
    required:true,
  },
  ingredients: [IngredientSchema],

});
CocktailSchema.plugin(idValidator);
const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;