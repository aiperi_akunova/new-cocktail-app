const mongoose = require('mongoose');
const config = require('./config');
const Cocktail = require("./models/Cocktail");
const User = require("./models/User");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url);
  const collection = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collection) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1,user2,user3]=await User.create({
    username:'john@gmail',
    password: 'doe',
    display_name: 'John Doe',
    token: nanoid(),
    role: 'admin',
    avatar: 'https://www.w3schools.com/howto/img_avatar2.png',
  }, {
    username:'tom@gmail',
    password: 'jerry',
    display_name: 'Tom Jerry',
    token: nanoid(),
    role: 'user',
    avatar: 'https://www.w3schools.com/howto/img_avatar2.png',
  },{
    username:'peri@gmail',
    password: '123',
    display_name: 'Perry',
    token: nanoid(),
    role: 'user',
    avatar: 'https://www.w3schools.com/howto/img_avatar2.png',
  });

  await Cocktail.create({
    user: user2,
      title: 'Margarita',
    image:'fixtures/margarita.jpg',
    description: 'The way margarita is made',
    published: false,
    ingredients: [
      {title: 'Lime juice', amount: '1 ounce'},
      {title: 'Orange liqueur', amount: '1/2 ounce'},
      {title: 'Blanco tequila', amount: '2 ounce'},
      {title: 'Agave syrup', amount: '1/2 ounce'},
    ]
    },{
    user: user3,
    title: 'Long Island',
    image:'fixtures/long-island.jpg',
    description: 'The way Long Island is made',
    published: false,
    ingredients: [
      {title: 'Vodka', amount: '3/4 ounce'},
      {title: 'White rum', amount: '3/4 ounce'},
      {title: 'Silver tequila', amount: '3/4 ounce'},
      {title: 'Gin', amount: '3/4 ounce'},
    ]
  })


  await mongoose.connection.close();
};

run().catch(console.error);