import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Cocktails from "./containers/Cocktails/Cocktails";
import OwnCocktails from "./containers/OwnCocktails/OwnCocktails";
import CocktailShow from "./containers/CocktailShow/CocktailShow";
import NewCocktailForm from "./containers/NewCocktailForm/NewCocktailForm";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Cocktails}/>
                <Route path="/add" component={NewCocktailForm}/>
                <ProtectedRoute path="/cocktails/:id/publish" isAllowed={user} redirectTo="/login" component={Login}/>
                <ProtectedRoute path="/cocktails/my" isAllowed={user} redirectTo="/login" component={OwnCocktails}/>
                <Route path="/cocktails/:id" component={CocktailShow}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;
