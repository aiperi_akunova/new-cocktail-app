import {Button, Card, CardMedia, Grid, makeStyles,} from "@material-ui/core";
import {apiURL} from "../../config";
import React from "react";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteCocktail, publishCocktail} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles({
    card: {
        height: '100%',
        textAlign: "center"
    },
    media: {
        paddingTop: '56.25%',
        height: 0,
        backgroundSize: "contain"
    },
})

const CocktailItem = ({title, picture, id, published}) => {
    const user = useSelector(state => state.users.user);
    const cocktails=useSelector(state => state.cocktails.cocktails)
    const dispatch=useDispatch();
    const classes = useStyles();
    const img=apiURL+'/'+ picture;
    console.log(id)

    return (
        <Grid item xs={8} sm={6} md={4} lg={2}>
            <Card className={classes.card}>
                <CardMedia
                    image={img}
                    className={classes.media}
                    title={title}
                />
                <h3>{title}</h3>
                <Button component={Link} to={'/cocktails/'+id}>Learn more</Button>
                {/*<Link to={'/cocktails/'+id}>Learn more</Link>*/}
                {user && user.role === 'admin' && (
                    <>
                        <Button
                            color={'primary'}
                            onClick={()=>dispatch(publishCocktail(id))}
                            disabled={published}
                        >Publish</Button>
                        <Button
                            color={'primary'}
                            onClick={()=>dispatch(deleteCocktail(id))}
                        >Delete</Button>
                    </>
                )}
            </Card>
        </Grid>
    );
};


export default CocktailItem;