import React from 'react';
import {Button, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import {apiURL} from "../../../../config";


const useStyles = makeStyles(theme =>({
    button: {
       borderLeft: "2px solid white",
        marginRight: "5px",
    },
    avatar: {
        width: "50px",
        height: "auto",
        borderRadius: "50%",
    },

}));

const UserMenu = ({user}) => {
    const classes= useStyles();
    const dispatch = useDispatch();
    return (
        <>
            {user && user.role !== 'admin' && (
                <Button color="inherit" component={Link} to='/cocktails/my' className={classes.button}>
                    My cocktails
                </Button>
            )}
            <Button color="inherit" onClick={() => dispatch(logoutUser())} className={classes.button}>
               Log out
            </Button>
            <Button color="inherit" className={classes.button}>
                {user.display_name}!
                <img src={user.avatar } alt='avatar' className={classes.avatar}/>
            </Button>
        </>
    );
};

export default UserMenu;