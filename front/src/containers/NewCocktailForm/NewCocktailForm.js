import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {createCocktail} from "../../store/actions/cocktailsActions";
import {useDispatch} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));




const NewCocktailForm = ({error, loading}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [ingredients, setIngredients] = useState([{
        title: '',
        amount: '',
    }]);

    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
    });

    console.log(state)
    console.log(ingredients)

    const addIngredient=()=>{
        setIngredients(prev=>[
            ...prev,
            {title: '', amount:''}])
    };

    const deleteIngredient=(id)=>{
        if(ingredients.length !== 0){
            setIngredients(ingredients.filter((ing,i)=> i !== id));
        }
    }

    const submitFormHandler = e => {
        e.preventDefault();
        const newState = {...state, ingredients: JSON.stringify(ingredients)}

        const formData = new FormData();
        Object.keys(newState).forEach(key => {
            formData.append(key, newState[key]);
        });

        console.log(formData)
        dispatch(createCocktail(formData));

    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const ingredientInputChange = (id,name,value)=>{
        setIngredients(prev =>{
            return ingredients.map((ing,i)=>{
                if(i === id){
                    return {...ing, [name]: value}
                }
                return ing;
            });
        })
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    }

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >

            <Grid item xs>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                  required
                />
            </Grid>
            <Grid item>
                {ingredients.map((ing, i) => (
                    <Grid container item alignItems={"flex-end"} key={i}>
                        <Grid item >
                            <TextField
                                required
                                variant="outlined"
                                label="Ingredient title"
                                name="title"
                                value={ingredients.title}
                                onChange={(e)=>ingredientInputChange(i,'title', e.target.value)}
                            />
                        </Grid>
                        <Grid item>
                            <TextField
                                required
                                variant="outlined"
                                label="Amount"
                                name="amount"
                                value={ingredients.amount}
                                onChange={(e)=>ingredientInputChange(i,'amount', e.target.value)}
                            />
                        </Grid>
                        <Grid item>
                            <button type='button' onClick={()=>deleteIngredient(i)}> X </button>
                        </Grid>
                    </Grid>
                ))}
                <Button variant={'outlined'} color={'primary'} onClick={addIngredient}> Add new ingredient</Button>
            </Grid>

            <Grid item xs>
                <TextField
                    fullWidth
                    multiline
                    rows={3}
                    variant="outlined"
                    label="Description"
                    name="description"
                    value={state.description}
                    onChange={inputChangeHandler}
                    required
                />
            </Grid>

            <Grid item xs>
                <TextField
                    type="file"
                    name="image"
                    onChange={fileChangeHandler}
                    required
                />
            </Grid>

            <Grid item xs>
                <Button type="submit" color="primary" variant="contained">Create</Button>
            </Grid>
        </Grid>
    );
};

export default NewCocktailForm;