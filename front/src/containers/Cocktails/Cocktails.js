import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, CircularProgress, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {fetchCocktails} from "../../store/actions/cocktailsActions";
import CocktailItem from "../../components/CocktailItem/CocktailItem";


const useStyles = makeStyles({
    box: {
        border: "1px solid darkblue",
    }
})



const Cocktails = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const loading = useSelector(state => state.cocktails.mainLoading);
    const dispatch = useDispatch();
    console.log(cocktails)

    useEffect(()=>{
        dispatch(fetchCocktails());
    },[dispatch]);

    return (
            <Grid container direction="column" spacing={2}>
            {user&& (
                <Grid item>
                    <Button coloe="primary" component={Link} to="/add">Add Cocktail</Button>
                </Grid>
            )}

                <Grid item>
                    <Grid item container justifyContent="center" direction="row" spacing={1}>
                        {loading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : cocktails.map(cocktail => (
                            <CocktailItem
                                key={cocktail._id}
                                id={cocktail._id}
                                className={classes.box}
                                title={cocktail.title} picture={cocktail.image}
                                published={cocktail.published}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
    );
};

export default Cocktails;