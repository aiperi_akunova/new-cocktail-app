import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, makeStyles} from "@material-ui/core";
import {fetchOwnCocktails} from "../../store/actions/cocktailsActions";
import CocktailItem from "../../components/CocktailItem/CocktailItem";


const useStyles = makeStyles({
    box: {
       border: "1px solid darkblue",
    }
})

const OwnCocktails = () => {
const classes=useStyles();
    const dispatch = useDispatch();
    const ownCocktails = useSelector(state => state.cocktails.ownCocktails);
    const loading = useSelector(state => state.cocktails.ownLoading);

    useEffect(() => {
        dispatch(fetchOwnCocktails());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {loading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : ownCocktails.map(cocktail => (
                        <CocktailItem
                            key={cocktail._id}
                            id={cocktail._id}
                            className={classes.box}
                            title={cocktail.title} picture={cocktail.image}/>
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default OwnCocktails;