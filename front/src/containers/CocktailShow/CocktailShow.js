import React, {useEffect} from 'react';
import {CircularProgress, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktail} from "../../store/actions/cocktailsActions";
import {apiURL} from "../../config";

const useStyles = makeStyles(()=>({
    img:{
        maxWidth: "250px",
        height: "auto"
    },
    paper:{
        padding: "15px"
    },
    box:{
        paddingLeft: "50px"
    },
    title:{
        textDecoration: "underline",
        color: "darkblue"
    }
}));


const CocktailShow = ({match}) => {
    const classes=useStyles();
    const dispatch = useDispatch();
    const cocktail = useSelector(state => state.cocktails.cocktail);
    const ingredients = cocktail.ingredients;
    const loading =useSelector(state => state.cocktails.cocktailLoading)

    useEffect(() => {
        dispatch(fetchCocktail(match.params.id));
    }, [dispatch, match.params.id]);

    return (
        <Paper elevation={3} className={classes.paper}>
            {loading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) :
                <>
            <Grid container>
                <Grid item>
                   <img src={apiURL+'/'+cocktail.image} className={classes.img}/>
                </Grid>
                <Grid item className={classes.box}>
                    <Typography variant="h3">{cocktail.title}</Typography>
                    <Typography variant="h5" className={classes.title}>Ingredients</Typography>
                    {ingredients && ingredients.map((ing,i)=>(
                        <Typography
                            variant="h6"
                            key={i}
                        >
                            {ing.title} : {ing.amount}</Typography>
                    ))}
                </Grid>
            </Grid>
                    <Grid>
                        <Typography variant="h5" className={classes.title}>Recipe:</Typography>
                        <Typography variant="subtitle1">{cocktail.description}</Typography>
                    </Grid>
                </>}
        </Paper>
    );
};

export default CocktailShow;