import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush, historyReplace} from "./historyActions";


export const FETCH_OWN_COCKTAILS_REQUEST = 'FETCH_OWN_COCKTAILS_REQUEST';
export const FETCH_OWN_COCKTAILS_SUCCESS = 'FETCH_OWN_COCKTAILS_SUCCESS';
export const FETCH_OWN_COCKTAILS_FAILURE = 'FETCH_OWN_COCKTAILS_FAILURE';

export const FETCH_COCKTAIL_REQUEST = 'FETCH_COCKTAIL_REQUEST';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const FETCH_COCKTAIL_FAILURE = 'FETCH_COCKTAIL_FAILURE';

export const FETCH_COCKTAILS_REQUEST = 'FETCH_COCKTAILS_REQUEST';
export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_COCKTAILS_FAILURE = 'FETCH_COCKTAILS_FAILURE';

export const CREATE_COCKTAIL_REQUEST = 'CREATE_COCKTAIL_REQUEST';
export const CREATE_COCKTAIL_SUCCESS = 'CREATE_COCKTAIL_FAILURE';
export const CREATE_COCKTAIL_FAILURE = 'CREATE_COCKTAIL_FAILURE';

export const PUBLISH_COCKTAIL_REQUEST = 'PUBLISH_COCKTAIL_REQUEST';
export const PUBLISH_COCKTAIL_SUCCESS = 'PUBLISH_COCKTAIL_SUCCESS';
export const PUBLISH_COCKTAIL_FAILURE = 'PUBLISH_COCKTAIL_FAILURE';


export const DELETE_COCKTAIL_REQUEST = 'DELETE_COCKTAIL_REQUEST';
export const DELETE_COCKTAIL_SUCCESS = 'DELETE_COCKTAIL_SUCCESS';
export const DELETE_COCKTAIL_FAILURE = 'DELETE_COCKTAIL_FAILURE';


export const deleteCocktailRequest = () => ({type: DELETE_COCKTAIL_REQUEST});
export const deleteCocktailSuccess = id => ({type: DELETE_COCKTAIL_SUCCESS, payload: id});
export const deleteCocktailFailure = () => ({type: DELETE_COCKTAIL_FAILURE});

export const publishCocktailRequest = () => ({type: PUBLISH_COCKTAIL_REQUEST});
export const publishCocktailSuccess = () => ({type: PUBLISH_COCKTAIL_SUCCESS});
export const publishCocktailFailure = error => ({type: PUBLISH_COCKTAIL_FAILURE, payload: error});

export const fetchCocktailRequest = () => ({type: FETCH_COCKTAIL_REQUEST});
export const fetchCocktailSuccess = cocktail => ({type: FETCH_COCKTAIL_SUCCESS, payload: cocktail});
export const fetchCocktailFailure = () => ({type: FETCH_COCKTAIL_FAILURE});

export const fetchOwnCocktailsRequest = () => ({type: FETCH_OWN_COCKTAILS_REQUEST});
export const fetchOwnCocktailsSuccess = ownCocktails => ({type: FETCH_OWN_COCKTAILS_SUCCESS, payload: ownCocktails});
export const fetchOwnCocktailsFailure = () => ({type: FETCH_OWN_COCKTAILS_FAILURE});

export const fetchCocktailsRequest = () => ({type: FETCH_COCKTAILS_REQUEST});
export const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAILS_SUCCESS, payload: cocktails});
export const fetchCocktailsFailure = () => ({type: FETCH_COCKTAILS_FAILURE});

export const createCocktailRequest = () => ({type: CREATE_COCKTAIL_REQUEST});
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});
export const createCocktailFailure = (error) => ({type: CREATE_COCKTAIL_FAILURE, payload: error});


export const fetchOwnCocktails = () => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(fetchOwnCocktailsRequest());
            const response = await axiosApi.get('/cocktails/my',{headers});
            dispatch(fetchOwnCocktailsSuccess(response.data));
        } catch (error) {
            toast.error('Could not fetch my cocktails!',{
                position: toast.POSITION.BOTTOM_LEFT
            });
            dispatch(fetchOwnCocktailsFailure());
        }
    };
};

export const fetchCocktails = () => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(fetchCocktailsRequest());
            const response = await axiosApi.get('/cocktails',{headers});
            dispatch(fetchCocktailsSuccess(response.data));
        } catch (error) {
            toast.error('Could not fetch cocktails!',{
                position: toast.POSITION.BOTTOM_LEFT
            });
            dispatch(fetchCocktailsFailure());
        }
    };
};


export const fetchCocktail = id => {
    return async dispatch => {
        try {
            dispatch(fetchCocktailRequest());
            const response = await axiosApi.get('/cocktails/' + id);
            console.log('response', response.data)
            dispatch(fetchCocktailSuccess(response.data));
        } catch (e) {
            dispatch(fetchCocktailFailure());
            toast.error('Could not fetch this product!',{
                position: toast.POSITION.BOTTOM_LEFT
            });
        }
    };
};


export const createCocktail = cocktailData => {
    return async dispatch => {
        try {
            dispatch(createCocktailRequest());
            await axiosApi.post('/cocktails', cocktailData);
            dispatch(createCocktailSuccess());
            dispatch(historyPush('/cocktails/my'));
            toast.success('Your cocktail is under review',{
                position: toast.POSITION.BOTTOM_LEFT
            });
        } catch (e) {
            dispatch(createCocktailFailure(e.response.data));
            toast.error('Could not create product',{
                position: toast.POSITION.BOTTOM_LEFT
            });
        }
    };
};

export const deleteCocktail = id => {
    return async (dispatch,getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };

            dispatch(deleteCocktailRequest());
            await axiosApi.delete('/cocktails/'+id, {headers});
            dispatch(deleteCocktailSuccess(id));
            toast.success('Successfully deleted');
            dispatch(historyReplace('/'));
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('Permission denied');
            }
            toast.error('Could not delete');
            dispatch(deleteCocktailFailure(error));
        }
    };
};



export const publishCocktail = id => {
    return async (dispatch) => {
        try {
            dispatch(publishCocktailRequest());
            await axiosApi.post('/cocktails/'+id+'/publish');
            dispatch(publishCocktailSuccess());
            toast.success('Successfully published',);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('Permission denied',{
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            dispatch(publishCocktailFailure(error));
        }
    };
};
