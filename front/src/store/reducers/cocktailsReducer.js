import {
    CREATE_COCKTAIL_FAILURE,
    CREATE_COCKTAIL_REQUEST,
    CREATE_COCKTAIL_SUCCESS, DELETE_COCKTAIL_FAILURE, DELETE_COCKTAIL_REQUEST, DELETE_COCKTAIL_SUCCESS,
    FETCH_COCKTAIL_FAILURE,
    FETCH_COCKTAIL_REQUEST,
    FETCH_COCKTAIL_SUCCESS,
    FETCH_COCKTAILS_FAILURE,
    FETCH_COCKTAILS_REQUEST,
    FETCH_COCKTAILS_SUCCESS,
    FETCH_OWN_COCKTAILS_FAILURE,
    FETCH_OWN_COCKTAILS_REQUEST,
    FETCH_OWN_COCKTAILS_SUCCESS
} from "../actions/cocktailsActions";

const initialState = {
    ownCocktails: [],
    ownLoading: false,
    cocktail: [],
    cocktailLoading: false,
    createLoading: false,
    error: null,
    cocktails: [],
    mainLoading: false,
    deleteLoading: false,
};

const cocktailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_OWN_COCKTAILS_REQUEST:
            return {...state, ownLoading: true};
        case FETCH_OWN_COCKTAILS_SUCCESS:
            return {...state,  ownLoading: false, ownCocktails: action.payload};
        case FETCH_OWN_COCKTAILS_FAILURE:
            return {...state, ownLoading: false};
        case FETCH_COCKTAIL_REQUEST:
            return {...state, cocktailLoading:true};
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktailLoading:false, cocktail: action.payload};
        case FETCH_COCKTAIL_FAILURE:
            return {...state, cocktailLoading:false};
        case FETCH_COCKTAILS_REQUEST:
            return {...state, mainLoading:true};
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, mainLoading:false, cocktails: action.payload};
        case FETCH_COCKTAILS_FAILURE:
            return {...state, mainLoading:false};
        case CREATE_COCKTAIL_REQUEST:
            return {...state, createLoading:true};
        case CREATE_COCKTAIL_SUCCESS:
            return {...state, createLoading:false, error: null};
        case CREATE_COCKTAIL_FAILURE:
            return {...state, createLoading:false, error: action.payload};
        case DELETE_COCKTAIL_REQUEST:
            return {...state, deleteLoading: true};
        case DELETE_COCKTAIL_SUCCESS:
            console.log('in reducer', action.payload)
            return {
                ...state,
                deleteLoading: false,
                cocktails: state.cocktails.filter(c=>c._id !== action.payload),
            }
        case DELETE_COCKTAIL_FAILURE:
            return {...state, deleteLoading: false}
        default:
            return state;
    }
};

export default cocktailsReducer;